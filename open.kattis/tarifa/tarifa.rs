use std::io::{stdin, BufRead};

fn main() {
    let stdin = stdin();
    let handle = stdin.lock();
    let mut input = handle.lines()
        .map(|r| r.unwrap().parse::<usize>().unwrap());

    while let Some(x) = input.by_ref().next() {
        let n = input.next().unwrap();
        let sum_p: usize = input.by_ref().take(n).sum();
        let output = x * (n + 1) - sum_p;
        println!("{}", output);
    }
}
