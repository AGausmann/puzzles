use std::collections::*;
use std::io::{stdin, BufRead};

fn main() {
    let stdin = stdin();
    let handle = stdin.lock();
    let mut lines = handle.lines().map(Result::unwrap);

    let line = lines.next().unwrap();
    let mut words = line.split_whitespace();
    let n1: usize = words.next().unwrap().parse().unwrap();
    let n2: usize = words.next().unwrap().parse().unwrap();
    let row1 = lines.next().unwrap();
    let row2 = lines.next().unwrap();
    let t: usize = lines.next().unwrap().parse().unwrap();
    assert_eq!(n1, row1.len());
    assert_eq!(n2, row2.len());

    let row1_set: BTreeSet<char> = row1.chars().collect();
    let row2_set: BTreeSet<char> = row2.chars().collect();

    let mut state: Vec<char> = row1.chars().rev()
        .chain(row2.chars())
        .collect();
    for _ in 0..t {
        state = (0..state.len() - 1).fold(state.clone(), |mut acc, i| {
            if row1_set.contains(&state[i]) && row2_set.contains(&state[i + 1]) {
                acc.swap(i, i + 1);
            }
            acc
        });
    }

    println!("{}", state.iter().collect::<String>());
}
