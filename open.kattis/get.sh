#!/bin/sh
# Utility to download and extract problem sample files from Open Kattis.

problem=${1-$(basename "$(pwd)")}

curl "https://open.kattis.com/problems/${problem}/file/statement/samples.zip" \
    | busybox unzip -
