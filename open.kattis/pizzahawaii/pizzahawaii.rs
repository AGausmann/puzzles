use std::collections::{BTreeMap, HashSet};
use std::io::{stdin, BufRead};

fn main() {
    let stdin = stdin();
    let handle = stdin.lock();
    let mut lines = handle.lines()
        .map(Result::unwrap);

    // Number of test cases.
    let t: usize = lines.next().unwrap()
        .parse().unwrap();

    for _i in 0..t {
        // Number of pizzas in this test case.
        let n: usize = lines.next().unwrap()
            .parse().unwrap();

        // Mapping each ingredient to the pizzas in which it is found.
        // Choosing BTreeMap here means keys will be sorted as they are
        // inserted, avoiding having to sort the output later.
        let mut foreign: BTreeMap<String, HashSet<usize>> = BTreeMap::new();
        let mut native: BTreeMap<String, HashSet<usize>> = BTreeMap::new();

        for j in 0..n {
            let _pizza_name = lines.next().unwrap();

            // Parse foreign ingredients for this pizza.
            for ingredient in lines.next().unwrap()
                .split_whitespace()
                .skip(1) // Unused input: Number of ingredients
            {
                if !foreign.contains_key(ingredient) {
                    foreign.insert(ingredient.to_string(), HashSet::new());
                }
                foreign.get_mut(ingredient).unwrap()
                    .insert(j);
            }

            // Parse native ingredients for this pizza.
            for ingredient in lines.next().unwrap()
                .split_whitespace()
                .skip(1) // Unused input: Number of ingredients
            {
                if !native.contains_key(ingredient) {
                    native.insert(ingredient.to_string(), HashSet::new());
                }
                native.get_mut(ingredient).unwrap()
                    .insert(j);
            }
        }

        // Select pairs in output, sorted first by foreign name, then native.
        for (f_name, f_pizzas) in &foreign {
            for (n_name, n_pizzas) in &native {
                if f_pizzas == n_pizzas {
                    println!("({}, {})", f_name, n_name);
                }
            }
        }
        println!();
    }
}
