use std::io::{stdin, BufRead};

fn main() {
    let stdin = stdin();
    let handle = stdin.lock();
    let mut lines = handle.lines()
        .map(Result::unwrap);

    // Parse header:
    let header = lines.next().unwrap();
    let mut words = header.split_whitespace();
    let capacity: usize = words.next().unwrap()
        .parse().unwrap();
    let num_stops: usize = words.next().unwrap()
        .parse().unwrap();

    let mut current_load = 0;
    for _i in 0..num_stops {
        // Parse current stop:
        let line = lines.next().unwrap();
        let mut words = line.split_whitespace();
        let num_off: usize = words.next().unwrap()
            .parse().unwrap();
        let num_on: usize = words.next().unwrap()
            .parse().unwrap();
        let num_waiting: usize = words.next().unwrap()
            .parse().unwrap();

        // Validate given numbers:
        if num_off > current_load {
            println!("impossible");
            return;
        }
        current_load -= num_off;
        current_load += num_on;
        if current_load > capacity {
            println!("impossible");
            return;
        }
        if num_waiting > 0 && current_load < capacity {
            println!("impossible");
            return;
        }
    }
    // Final condition:
    if current_load > 0 {
        println!("impossible");
        return;
    }
    println!("possible");
}
