use std::collections::*;
use std::io::{stdin, BufRead};

fn main() {
    let stdin = stdin();
    let handle = stdin.lock();
    let mut lines = handle.lines().map(Result::unwrap);

    let line = lines.next().unwrap();
    let mut words = line.split_whitespace();
    let n: usize = words.next().unwrap().parse().unwrap();
    let s: usize = words.next().unwrap().parse().unwrap();
    let t: usize = words.next().unwrap().parse().unwrap();

    let graph: Vec<Vec<usize>> = lines
        .map(|line| {
            line.split_whitespace()
                .map(|word| word.parse().unwrap())
                .collect()
        })
        .collect();

    assert_eq!(graph.len(), n);
    for row in &graph {
        assert_eq!(row.len(), n);
    }

    let mut closed = BTreeSet::new();
    let mut open = BTreeMap::new();
    open.insert(s, 0);
    loop {
        let (&next, &cost) = open.iter()
            .min_by_key(|(_, x)| *x)
            .unwrap();

        open.remove(&next);
        closed.insert(next);

        if next == t {
            println!("{}", cost);
            break;
        }

        for i in 0..n {
            if i == next || closed.contains(&i) {
                continue;
            }
            let neighbor_cost = cost + graph[next][i];
            if let Some(&x) = open.get(&i) {
                if neighbor_cost < x {
                    open.insert(i, neighbor_cost);
                }
            } else {
                open.insert(i, neighbor_cost);
            }
        }
    }
}
