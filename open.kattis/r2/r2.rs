use std::io::{stdin, BufRead};

fn main() {
    let stdin = stdin();
    let handle = stdin.lock();
    
    for line in handle.lines().map(Result::unwrap) {
        let mut words = line.split_whitespace();
        let r1: isize = words.next().unwrap().parse().unwrap();
        let s: isize = words.next().unwrap().parse().unwrap();
        let r2 = s * 2 - r1;
        println!("{}", r2);
    }
}
