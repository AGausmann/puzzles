use std::io::{stdin, BufRead};

fn decode(gray: u64) -> u64 {
    (0..64).rev()
        .fold(gray, |acc, i| (((1 << i) & acc) >> 1) ^ acc)
}

fn main() {
    let stdin = stdin();
    let handle = stdin.lock();

    for line in handle.lines().map(Result::unwrap) {
        let mut words = line.split_whitespace();
        let _n: usize = words.next().unwrap().parse().unwrap();
        let a = u64::from_str_radix(words.next().unwrap(), 2).unwrap();
        let b = u64::from_str_radix(words.next().unwrap(), 2).unwrap();

        println!("{}", decode(b) - decode(a) - 1);
    }
}
