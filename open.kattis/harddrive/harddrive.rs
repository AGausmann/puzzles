use std::collections::*;
use std::io::{stdin, BufRead};

fn main() {
    let stdin = stdin();
    let handle = stdin.lock();
    let mut lines = handle.lines().map(Result::unwrap);

    let line = lines.next().unwrap();
    let mut words = line.split_whitespace();
    let n: usize = words.next().unwrap().parse().unwrap();
    let c: usize = words.next().unwrap().parse().unwrap();
    let b: usize = words.next().unwrap().parse().unwrap();
    let broken: BTreeSet<usize> = lines.next().unwrap()
        .split_whitespace()
        .map(|line| line.parse::<usize>().unwrap() - 1)
        .collect();
    assert_eq!(broken.len(), b);

    let mut bits = vec![false; n];
    let is_broken: Vec<bool> = (0..n)
        .map(|i| broken.contains(&i))
        .collect();

    let mut changed = 0;
    if c & 1 == 1 {
        bits[0] = true;
        changed += 1;
    }
    for i in 1..bits.len() {
        if changed == c {
            break;
        }
        if is_broken[i] {
            continue;
        }
        if bits[i - 1] == false {
            bits[i] = true;
            changed += 2;
        }
    }
    assert_eq!(changed, c);
    assert!(broken.iter().all(|&i| bits[i] == false));

    let result = bits.iter().fold(
        String::with_capacity(bits.len()),
        |mut acc, x| {
            acc.push(match x {
                true => '1',
                false => '0',
            });
            acc
        });

    println!("{}", result);
}
