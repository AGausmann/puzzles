extern crate dp_2018_08_20 as dp;

use dp::challenge::bonus_2;

fn main() {
    let mut bonus_2 = bonus_2()
        .iter()
        .cloned()
        .collect::<Vec<_>>();

    bonus_2.sort();

    println!("{} matches:", bonus_2.len());
    for word in &bonus_2 {
        println!("    {}", word);
    }
}
