use std::collections::HashSet;
use enable1::{self, ENABLE1};

pub fn funnel(src: &str, dest: &str) -> bool {
    // A funneled word must always be one less in length.
    if src.len() != dest.len() + 1 {
        return false;
    }
    let mut src = src.chars();
    let mut dest = dest.chars();

    while let (Some(s), Some(d)) = (src.next(), dest.next()) {
        // Find the first mismatched character
        if s != d {
            let s = src.next().unwrap();

            // The next character in src must match this character of dest.
            return s == d

            // .. and the rest of src must match the rest of dest.
                && src.eq(dest);
        }
    }
    // No mismatch found, then the last letter was skipped:
    true
}

pub fn bonus(src: &str) -> HashSet<String> {
    // Build the list of candidates first,
    // then check for their existence in enable1.

    (0..src.chars().count())

        // For each character at index i ...
        .map(|i| {
            // Take everything up to (not including) that character,
            // concatenate (chain) everything after the character.
            src.chars().take(i)
                .chain(src.chars().skip(i + 1))

            // We can skip collection into a string; tries only require an
            // iterator of keys (in this case, characters).
        })

        // Check if it exists in enable1.
        .filter(|chars| ENABLE1.contains(chars.clone()))

        // Finally, gather the results.
        .map(|chars| chars.collect())
        .collect()
}

pub fn bonus_2() -> HashSet<&'static str> {
    // Bonus 1 was efficient enough to be applied to all of enable1 in about
    // a second ...

    enable1::words()
        .filter(|&word| bonus(word).len() == 5)
        .collect()
}

#[test]
fn test_challenge_examples() {
    assert!(funnel("leave", "eave"));
    assert!(funnel("reset", "rest"));
    assert!(funnel("dragoon", "dragon"));
    assert!(! funnel("eave", "leave"));
    assert!(! funnel("sleet", "lets"));
    assert!(! funnel("skiff", "ski"));
}

#[cfg(test)]
macro_rules! hash_set {
    ($($x:expr),*) => {
        [$($x),*].iter().cloned().collect::<HashSet<_>>()
    }
}

#[test]
fn test_bonus_examples() {
    assert_eq!(
        bonus("dragoon"),
        hash_set!{
            "dragon".to_string()
        }
    );
    assert_eq!(
        bonus("boats"),
        hash_set!{
            "oats".to_string(),
            "bats".to_string(),
            "bots".to_string(),
            "boas".to_string(),
            "boat".to_string()
        }
    );
    assert!(bonus("affidavit").is_empty());
}
