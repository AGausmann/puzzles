# Rust

Got a bit behind this week; this has been the first week of classes at
university. Just catching up on the problems I missed.

As usual, I will post the full solution in my [GitLab repository]
(https://gitlab.com/agausmann/dailyprogrammer), I will include the
most relevant bits here.

## Main challenge

A very simple, almost trivial algorithm. For one word to be a "funneled"
word of another, it must be missing a single letter from the source
word. Up until that character, the words match, but once a mismatch has
been found, the rest of the source and target strings must also match
after skipping that missing character. If you've exhausted the target
string's length, then that means the last character of the source was
missing.

    pub fn funnel(src: &str, dest: &str) -> bool {
        // A funneled word must always be one less in length.
        if src.len() != dest.len() + 1 {
            return false;
        }
        let mut src = src.chars();
        let mut dest = dest.chars();

        while let (Some(s), Some(d)) = (src.next(), dest.next()) {
            // Find the first mismatched character
            if s != d {
                let s = src.next().unwrap();

                // The next character in src must match this character of dest.
                return s == d

                // .. and the rest of src must match the rest of dest.
                    && src.eq(dest);
            }
        }
        // No mismatch found, then the last letter was skipped:
        true
    }

## Bonus 1

### Preface: parsing enable1

Many of the problems posted recently on r/dailyprogrammer have involved
using the enable1 dictionary. I've written an implementation of the
[trie](https://en.wikipedia.org/wiki/Trie) data structure which is good at
efficiently storing streaming, variable-length keys (for example, strings),
and I've gotten to use it for many of my recent solutions. Once again,
I used it here for efficiently storing and querying the enable1 dictionary.

The implementation details of the trie and the enable1 dictionary parser
are in the repository I linked to before; this weekend I'll be taking the
time to refactor duplicate code and fully document the common resources.
If you can't find it, check again later and read the README ;)

### Solution

    use std::collections::HashSet;
    use enable1::ENABLE1;

    pub fn bonus(src: &str) -> HashSet<String> {
        // Build the list of candidates first,
        // then check for their existence in enable1.

        (0..src.chars().count())

            // For each character at index i ...
            .map(|i| {
                // Take everything up to (not including) that character,
                // concatenate (chain) everything after the character.
                src.chars().take(i)
                    .chain(src.chars().skip(i + 1))

                // We can skip collection into a string; tries only require an
                // iterator of keys (in this case, characters).
            })

            // Check if it exists in enable1.
            .filter(|chars| ENABLE1.contains(chars.clone()))

            // Finally, gather the results.
            .map(|chars| chars.collect())
            .collect()
    }

## Bonus 2

### Solution

    use std::collections::HashSet;
    use enable1;

    pub fn bonus_2() -> HashSet<&'static str> {
        // Bonus 1 was efficient enough to be applied to all of enable1 in about
        // a second ...

        enable1::words()
            .filter(|&word| bonus(word).len() == 5)
            .collect()
    }

### Results

    28 matches:
        beasts
        boats
        brands
        chards
        charts
        clamps
        coasts
        cramps
        drivers
        grabblers
        grains
        grippers
        moats
        peats
        plaints
        rousters
        shoots
        skites
        spates
        spicks
        spikes
        spines
        teats
        tramps
        twanglers
        waivers
        writes
        yearns
