// r/dailyprogrammer #363 [Intermediate] Word Hy-phen-a-tion By Com-put-er
// author: Adam Gausmann (u/ninja_tokumei)
// language: Rust

use std::collections::HashMap;
use std::fs::File;
use std::hash::Hash;
use std::io::{stdin, BufRead, BufReader};
use std::iter::FromIterator;
use std::marker::PhantomData;
use std::str::FromStr;

/// The node used by `PrefixTreeMap`.
#[derive(Debug)]
pub struct Node<K, V>
where
    K: Eq + Hash,
{
    next: HashMap<K, Node<K, V>>,
    value: Option<V>,
}

impl<K, V> Node<K, V>
where
    K: Eq + Hash,
{
    pub fn new() -> Node<K, V> {
        Node {
            next: HashMap::new(),
            value: None,
        }
    }
    
    pub fn with_value(value: V) -> Node<K, V> {
        Node {
            next: HashMap::new(),
            value: Some(value),
        }
    }

    pub fn get(&self, key: &K) -> Option<&Node<K, V>> {
        self.next.get(key)
    }

    pub fn get_mut(&mut self, key: &K) -> Option<&mut Node<K, V>> {
        self.next.get_mut(key)
    }

    pub fn insert(&mut self, key: K, node: Node<K, V>) -> Option<Node<K, V>> {
        self.next.insert(key, node)
    }

    pub fn exists(&self, key: &K) -> bool {
        self.get(key).is_some()
    }

    pub fn value(&self) -> &Option<V> {
        &self.value
    }

    pub fn value_mut(&mut self) -> &mut Option<V> {
        &mut self.value
    }
}

/// An implementation of a prefix tree, or _trie_, that can map a value to any
/// node in the tree. This is the recommended ADT if values need to be stored
/// and indexed by a sequence of keys.
///
/// Internally, a linked k-ary tree is used, with each node owning a map to
/// each of their child nodes given the next key in the sequence.
#[derive(Debug)]
pub struct PrefixTreeMap<K, V>
where
    K: Eq + Hash,
{
    root: Node<K, V>,
    _k: PhantomData<K>,
}

impl<K, V> PrefixTreeMap<K, V>
where
    K: Eq + Hash,
{
    pub fn new() -> PrefixTreeMap<K, V> {
        PrefixTreeMap {
            root: Node::new(),
            _k: PhantomData,
        }
    }

    pub fn root(&self) -> &Node<K, V> {
        &self.root
    }

    pub fn root_mut(&mut self) -> &mut Node<K, V> {
        &mut self.root
    }

    pub fn get<I>(&self, iter: I) -> Option<&Node<K, V>>
    where
        I: IntoIterator<Item=K>,
    {
        iter.into_iter()
            .fold(Some(self.root()), |node, key| { 
                node.and_then(|n| n.get(&key))
            })
    }

    pub fn get_mut<I>(&mut self, iter: I) -> Option<&mut Node<K, V>>
    where
        I: IntoIterator<Item=K>,
    {
        iter.into_iter()
            .fold(Some(self.root_mut()), |node, key| {
                node.and_then(|n| n.get_mut(&key))
            })
    }

    pub fn insert<I>(&mut self, iter: I, node: Node<K, V>)
    where
        I: IntoIterator<Item=K>,
        K: Clone,
    {
        let old_node = iter.into_iter()
            .fold(self.root_mut(), |node, key| {
                if !node.exists(&key) {
                    node.insert(key.clone(), Node::new());
                }
                node.get_mut(&key).unwrap()
            });

        *old_node = node;
    }
}

impl<K, V, I> FromIterator<(I, V)> for PrefixTreeMap<K, V>
where
    I: IntoIterator<Item=K>,
    K: Clone + Eq + Hash,
{
    fn from_iter<T>(iter: T) -> Self
    where
        T: IntoIterator<Item=(I, V)>,
    {
        let mut map = PrefixTreeMap::new();
        for (i, v) in iter {
            map.insert(i, Node::with_value(v));
        }
        map
    }
}

/// The unit of the pattern's index sequence.
type PatternKey = char;

/// The pattern type as defined by the problem; stores each matching
/// character alongside its weight (the optional digit _before_ it).
///
/// The default weight if none is specified has been chosen to be zero `0`
/// since it does not appear in the given pattern dictionary, it is the
/// least possible value, and it obeys the rule of no hyphenation for even
/// numbers.
///
/// `weights` MAY have one additional element that, if present, indicates
/// the weight of the character _after_ the last match.
#[derive(Debug)]
struct Pattern {
    base: String,
    weights: Vec<u8>,
}

impl Pattern {
    fn base(&self) -> &str {
        &self.base
    }

    fn weights(&self) -> &[u8] {
        &self.weights
    }
}

#[derive(Debug)]
enum Impossible {}

impl FromStr for Pattern {
    type Err = Impossible;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut chars = s.chars();
        let mut next = chars.next();

        let mut base = String::with_capacity(s.len());
        let mut weights = Vec::with_capacity(s.len());

        while let Some(c) = next {
            if c.is_ascii_digit() {
                weights.push(c.to_digit(10).unwrap() as u8);
                next = chars.next();
            } else {
                weights.push(0);
            }
            if let Some(c) = next {
                base.push(c);
                next = chars.next();
            }
        }

        base.shrink_to_fit();
        weights.shrink_to_fit();

        Ok(Pattern {
            base,
            weights,
        })
    }
}

/// Algorithm implementation for the problem.
///
/// Walks along the string, matching patterns as it goes. As matches
/// are encountered, the result weights for each character are adjusted.
/// At the end, hyphens are inserted before the characters with odd weights.
fn hyphenate(s: &str, patterns: &PrefixTreeMap<PatternKey, Pattern>)
    -> String
{
    // Terminate the string on either side with pattern anchors.
    let s = format!(".{}.", s);

    let mut nodes: Vec<&Node<PatternKey, Pattern>> = Vec::new();
    let mut weights = vec![0u8; s.len()];

    // Walk the string.
    for (i, c) in s.chars().enumerate() {
        // Retain and walk down subtrees that still match.
        let next = nodes.drain(..)
            .filter_map(|node| node.get(&c))
            .collect();
        nodes = next;

        // See if we can start a new match.
        if let Some(node) = patterns.root().get(&c) {
            nodes.push(node);
        }

        // See if we have exhaustively matched any patterns.
        for &node in &nodes {
            if let &Some(ref pattern) = node.value() {
                let n = pattern.base().len();

                for (j, &x) in pattern.weights().iter().enumerate() {
                    let w = &mut weights[1 + i + j - n];

                    if x > *w {
                        *w = x
                    }
                }
            }
        }
    }

    let hyphens = weights.iter()
        .enumerate()
        .filter(|(_, &x)| x & 1 == 1)
        .map(|t| t.0)
        .filter(|&x| x > 1 && x < s.len() - 1);
    let mut out = String::new();
    let mut i = 1;
    for j in hyphens {
        out.push_str(&s[i..j]);
        out.push('-');
        i = j;
    }
    out.push_str(&s[i..(s.len() - 1)]);
    out
}

fn main() {
    let patterns_file = File::open("tex-hyphenation-patterns.txt")
        .expect("Unable to open patterns file.");
    let reader = BufReader::new(patterns_file);
    let pattern_tree: PrefixTreeMap<PatternKey, Pattern>  = reader.lines()
        .map(|result| result.expect("Error while reading patterns file."))
        .map(|line| {
            let pattern: Pattern = line.parse().unwrap();
            (pattern.base().to_string().chars().collect::<Vec<char>>(), pattern)
        })
        .collect();

    let stdin = stdin();
    let handle = stdin.lock();
    let mut tally = vec![0usize; 10];

    for line in handle.lines().map(Result::unwrap) {
        let out = hyphenate(&line, &pattern_tree);
        println!("{}", &out);
        let n = out.chars()
            .filter(|&c| c == '-')
            .count();
        if n < tally.len() {
            tally[n] += 1;
        }
    }

    eprintln!("Summary: {:?}", &tally);
}
