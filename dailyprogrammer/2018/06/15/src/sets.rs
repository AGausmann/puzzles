use std::cmp;
use std::collections::BTreeSet;
use std::ops;

pub const CHARACTER_SET_LEN: usize = 26;

pub fn ctoi(c: char) -> usize {
    match c {
        'a'...'z' => (c as usize) - 0x61,
        _ => panic!("unsupported character: {}", c),
    }
}

pub fn itoc(x: usize) -> char {
    let ord: u32 = match x {
        0...25 => (x + 0x61) as u32,
        _ => panic!("unknown character id: {}", x),
    };
    ::std::char::from_u32(ord)
        .unwrap()
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CharacterSet {
    pub freq: [usize; CHARACTER_SET_LEN],
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct AnagramSlice {
    pub set: CharacterSet,
    pub position: isize,
    pub stride: isize,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct AnagramSet {
    pub set: CharacterSet,
    pub members: BTreeSet<isize>,
}

#[derive(Debug, Clone)]
pub struct PackedSets {
    pub sets: Vec<AnagramSet>,
}

impl CharacterSet {
    pub fn len(&self) -> usize {
        self.freq.iter().sum()
    }
}

impl<'a, 'b> ops::BitAnd<&'b CharacterSet> for &'a CharacterSet {
    type Output = CharacterSet;

    fn bitand(self, other: &'b CharacterSet) -> CharacterSet {
        let freq_vec: Vec<usize> = self.freq.iter()
            .zip(other.freq.iter())
            .map(|(x, y)| *cmp::min(x, y))
            .collect();
        debug_assert_eq!(freq_vec.len(), CHARACTER_SET_LEN);

        let mut freq = [0; CHARACTER_SET_LEN];
        for (x, y) in freq.iter_mut().zip(freq_vec.iter()) {
            *x = *y;
        }

        CharacterSet {
            freq,
        }
    }
}

impl<'a, 'b> ops::Sub<&'b CharacterSet> for &'a CharacterSet {
    type Output = CharacterSet;

    fn sub(self, other: &'b CharacterSet) -> CharacterSet {
        let freq_vec: Vec<usize> = self.freq.iter()
            .zip(other.freq.iter())
            .map(|(&x, &y)| x.saturating_sub(y))
            .collect();
        debug_assert_eq!(freq_vec.len(), CHARACTER_SET_LEN);

        let mut freq = [0; CHARACTER_SET_LEN];
        for (x, y) in freq.iter_mut().zip(freq_vec.iter()) {
            *x = *y;
        }

        CharacterSet {
            freq,
        }
    }
}

impl AnagramSet {
    pub fn len(&self) -> usize {
        assert_eq!(self.members.len(), self.set.len());
        self.set.len()
    }

    pub fn compatible_with(&self, other: &AnagramSet) -> bool {
        let set = &self.set & &other.set;
        let members = &self.members & &other.members;

        set.len() == members.len()
    }
}

impl<'a, 'b> ops::BitAnd<&'b AnagramSet> for &'a AnagramSet {
    type Output = Option<AnagramSet>;

    fn bitand(self, other: &'b AnagramSet) -> Option<AnagramSet> {
        let set = &self.set & &other.set;
        let members = &self.members & &other.members;

        if members.len() == set.len() {
            Some(AnagramSet {
                set,
                members,
            })
        } else {
            None
        }
    }
}

impl<'a, 'b> ops::Sub<&'b AnagramSet> for &'a AnagramSet {
    type Output = Option<AnagramSet>;

    fn sub(self, other: &'b AnagramSet) -> Option<AnagramSet> {
        let set = &self.set - &other.set;
        let members = &self.members - &other.members;

        if members.len() == set.len() {
            Some(AnagramSet {
                set,
                members,
            })
        } else {
            None
        }
    }
}

impl<'a> From<&'a AnagramSlice> for AnagramSet {
    fn from(slice: &'a AnagramSlice) -> AnagramSet {
        let members = (0..slice.set.len())
            .map(|x| slice.position + slice.stride * (x as isize))
            .collect();

        AnagramSet {
            set: slice.set.clone(),
            members,
        }
    }
}

impl PackedSets {
    /// Adds another set, applying it as a constraint by intersecting it with
    /// all added sets.
    pub fn add(&mut self, mut set: AnagramSet) -> Result<(), AnagramSet> {
        let mut new_sets = Vec::with_capacity(self.sets.len() * 2 + 1);
        for other in &self.sets {
            let maybe_intersection = &set & other;
            if let Some(intersection) = maybe_intersection {
                let leftover = (&set - other).unwrap();
                let new_other = (other - &set).unwrap();

                set = leftover;
                if intersection.len() != 0 {
                    new_sets.push(intersection);
                }
                if new_other.len() != 0 {
                    new_sets.push(new_other);
                }

                // It's pointless to try to further intersect an empty set:
                if set.len() == 0 {
                    break;
                }
            } else {
                return Err(set);
            }
        }
        // Check if there are leftovers that have to be saved.
        if set.len() != 0 {
            new_sets.push(set);
        }
        self.sets = new_sets;
        Ok(())
    }
}
