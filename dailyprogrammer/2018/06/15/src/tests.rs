#![cfg(test)]

use sets::*;

macro_rules! btreeset {
    ($($x:expr),*) => {
        {
            use std::collections::BTreeSet;
            let mut set = BTreeSet::new();
            $(
                set.insert($x);
            )*
            set
        }
    }
}


const CSET_A: CharacterSet = CharacterSet {
    freq: [5, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
};
const CSET_B: CharacterSet = CharacterSet {
    freq: [2, 4, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
};

const ASLICE_A: AnagramSlice = AnagramSlice {
    set: CSET_A,
    position: 1,
    stride: 6,
};

const ASLICE_B: AnagramSlice = AnagramSlice {
    set: CSET_B,
    position: 3,
    stride: 4,
};

#[test]
fn character_intersect_values() {
    assert_eq!(&CSET_A & &CSET_B, CharacterSet {
        freq: [2, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    });
}

#[test]
fn character_intersect_commutative() {
    assert_eq!(&CSET_A & &CSET_B, &CSET_B & &CSET_A);
}

#[test]
fn character_difference_values() {
    assert_eq!(&CSET_A - &CSET_B, CharacterSet {
        freq: [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    });
}

#[test]
fn character_difference_noncommutative() {
    assert_ne!(&CSET_A - &CSET_B, &CSET_B - &CSET_A);
}

#[test]
fn anagram_slice_to_set_values() {
    assert_eq!(AnagramSet::from(&ASLICE_A), AnagramSet {
        set: CSET_A,
        members: btreeset![1, 7, 13, 19, 25, 31, 37, 43, 49, 55],
    });
}

#[test]
fn anagram_set_intersect_incompatible() {
    let aset_a = AnagramSet::from(&ASLICE_A);
    let aset_b = AnagramSet::from(&ASLICE_B);

    assert!(!aset_a.compatible_with(&aset_b));
}

#[test]
fn anagram_set_intersect_none() {
    let aset_a = AnagramSet::from(&ASLICE_A);
    let aset_b = AnagramSet::from(&ASLICE_B);

    assert_eq!(&aset_a & &aset_b, None);
}
