use std::sync::mpsc;
use std::thread::JoinHandle;

use sets::{AnagramSlice, PackedSets};

pub struct SlicePacker {
    handle: JoinHandle<()>,
    incoming: mpsc::Receiver<PackedSets>,
    finish_tx: mpsc::Sender<()>,
}

impl SlicePacker {
    pub fn new(slices: Vec<AnagramSlice>) -> SlicePacker {
        unimplemented!()
    }

    pub fn finish(self) -> Vec<PackedSets> {
        self.finish_tx.send(()).unwrap();
        self.handle.join().unwrap();
        return self.incoming.iter().collect()
    }
}

impl Iterator for SlicePacker {
    type Item = PackedSets;

    fn next(&mut self) -> Option<PackedSets> {
        self.incoming.iter().next()
    }
}
