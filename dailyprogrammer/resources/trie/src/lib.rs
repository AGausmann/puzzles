use std::collections::HashMap;
use std::hash::Hash;
use std::iter::FromIterator;
use std::mem;

/// Contains a trie node's value (if any at this location), and pointers to
/// its children.
#[derive(Debug, Clone, PartialEq, Eq)]
struct TrieNode<K, V>
where
    K: Eq + Hash,
{
    value: Option<V>,
    next: HashMap<K, TrieNode<K, V>>,
}

impl<K, V> TrieNode<K, V>
where
    K: Eq + Hash,
{
    fn new() -> TrieNode<K, V> {
        TrieNode {
            value: None,
            next: HashMap::new(),
        }
    }
}

/// A map that stores values indexed by a stream of keys.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TrieMap<K, V>
where
    K: Eq + Hash,
{
    root: TrieNode<K, V>,
}

impl<K, V> TrieMap<K, V>
where
    K: Eq + Hash,
{
    pub fn new() -> TrieMap<K, V> {
        TrieMap {
            root: TrieNode::new(),
        }
    }

    fn node<I>(&self, keys: I) -> Option<&TrieNode<K, V>>
    where
        I: IntoIterator<Item=K>,
    {
        keys.into_iter()
            .fold(Some(&self.root), |maybe_node, key| {
                maybe_node.and_then(|node| node.next.get(&key))
            })
    }

    fn node_mut<I>(&mut self, keys: I) -> Option<&mut TrieNode<K, V>>
    where
        I: IntoIterator<Item=K>,
    {
        keys.into_iter()
            .fold(Some(&mut self.root), |maybe_node, key| {
                maybe_node.and_then(|node| node.next.get_mut(&key))
            })
    }

    fn make_node<I>(&mut self, keys: I) -> &mut TrieNode<K, V>
    where
        I: IntoIterator<Item=K>,
    {
        keys.into_iter()
            .fold(&mut self.root, |node, key| {
                node.next.entry(key).or_insert(TrieNode::new())
            })
    }

    pub fn get<I>(&self, keys: I) -> Option<&V>
    where
        I: IntoIterator<Item=K>,
    {
        self.node(keys)
            .and_then(|node| node.value.as_ref())
    }

    pub fn get_mut<I>(&mut self, keys: I) -> Option<&mut V>
    where
        I: IntoIterator<Item=K>,
    {
        self.node_mut(keys)
            .and_then(|node| node.value.as_mut())
    }

    pub fn contains<I>(&self, keys: I) -> bool
    where
        I: IntoIterator<Item=K>,
    {
        self.get(keys).is_some()
    }

    pub fn insert<I>(&mut self, keys: I, value: V) -> Option<V>
    where
        I: IntoIterator<Item=K>,
    {
        mem::replace(&mut self.make_node(keys).value, Some(value))
    }

    pub fn remove<I>(&mut self, keys: I) -> Option<V>
    where
        I: IntoIterator<Item=K>,
    {
        self.node_mut(keys)
            .and_then(|node| mem::replace(&mut node.value, None))
    }

    pub fn clear(&mut self) {
        self.root = TrieNode::new();
    }
}

impl<I, V> FromIterator<(I, V)> for TrieMap<I::Item, V>
where
    I: IntoIterator,
    I::Item: Eq + Hash,
{
    fn from_iter<T>(iter: T) -> Self
    where
        T: IntoIterator<Item=(I, V)>,
    {
        let mut map = TrieMap::new();
        for (keys, value) in iter.into_iter() {
            map.insert(keys, value);
        }
        map
    }
}

pub struct TrieSet<K>
where
    K: Eq + Hash,
{
    map: TrieMap<K, ()>,
}

impl<K> TrieSet<K>
where
    K: Eq + Hash,
{
    pub fn new() -> TrieSet<K> {
        TrieSet {
            map: TrieMap::new(),
        }
    }

    pub fn from_map(map: TrieMap<K, ()>) -> TrieSet<K> {
        TrieSet {
            map,
        }
    }

    pub fn contains<I>(&self, keys: I) -> bool
    where
        I: IntoIterator<Item=K>
    {
        self.map.contains(keys)
    }

    pub fn insert<I>(&mut self, keys: I) -> bool
    where
        I: IntoIterator<Item=K>,
    {
        self.map.insert(keys, ()).is_none()
    }
    
    pub fn remove<I>(&mut self, keys: I) -> bool
    where
        I: IntoIterator<Item=K>,
    {
        self.map.remove(keys).is_some()
    }

    pub fn clear(&mut self) {
        self.map.clear();
    }
}

impl<K> From<TrieMap<K, ()>> for TrieSet<K>
where
    K: Eq + Hash,
{
    fn from(map: TrieMap<K, ()>) -> TrieSet<K> {
        TrieSet::from_map(map)
    }
}

impl<I> FromIterator<I> for TrieSet<I::Item>
where
    I: IntoIterator,
    I::Item: Eq + Hash,
{
    fn from_iter<T>(iter: T) -> Self
    where
        T: IntoIterator<Item=I>
    {
        iter.into_iter()
            .map(|key| (key, ()))
            .collect::<TrieMap<_, _>>()
            .into()
    }
}
