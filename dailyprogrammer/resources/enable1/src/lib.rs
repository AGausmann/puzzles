#[macro_use]
extern crate lazy_static;
extern crate trie;

use std::str::Lines;
use trie::TrieSet;

static ENABLE1_TEXT: &'static str = include_str!("enable1.txt");

pub type Words = Lines<'static>;

pub fn words() -> Words {
    ENABLE1_TEXT.lines()
}

lazy_static! {
    pub static ref ENABLE1: TrieSet<char> = {
        words()
            .map(|word| word.chars())
            .collect()
    };
}
