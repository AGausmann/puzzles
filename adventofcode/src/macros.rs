#![allow(unused_macros)]

/// Performs common setup tasks for dealing with map-based problems where the map can
/// be expressed as a finite set of states (like characters on a screen).
///
/// Accepts identifiers for map, point, and cell types, along with a set of mappings
/// from characters to cell variant names. Provides conversion trait implementations
/// between the cell type and character type, indexing operations on the map with the
/// point type, and a fancy `Display` implementation that draws the map in console.
macro_rules! charmap {
    ($map:ident; $point:ident; $cell:ident {$($c:expr => $name:ident),*}; ) => {

        #[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
        pub enum $cell {$(
            $name,
        )*}

        impl From<char> for $cell {
            fn from(c: char) -> $cell {
                match c {
                    $(
                        $c => $cell::$name,
                    )*
                    unknown => panic!("Unexpected character {:?}", unknown),
                }
            }
        }

        impl From<$cell> for char {
            fn from(c: $cell) -> char {
                match c {
                    $(
                        $cell::$name => $c,
                    )*
                }
            }
        }

        impl std::fmt::Display for $cell {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                write!(f, "{}", char::from(*self))
            }
        }

        #[derive(Debug, Clone, PartialEq, Eq)]
        pub struct $map(pub Vec<Vec<$cell>>);

        impl std::fmt::Display for $map {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                for row in &self.0 {
                    for &cell in row {
                        write!(f, "{}", cell)?;
                    }
                    writeln!(f)?;
                }
                Ok(())
            }
        }

        #[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
        pub struct $point(pub usize, pub usize);

        impl std::fmt::Display for $point {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                write!(f, "({}, {})", self.0, self.1)
            }
        }

        impl std::ops::Index<$point> for $map {
            type Output = $cell;

            fn index(&self, $point(i, j): $point) -> &$cell {
                &self.0[i][j]
            }
        }
        
        impl std::ops::IndexMut<$point> for $map {
            fn index_mut(&mut self, $point(i, j): $point) -> &mut $cell {
                &mut self.0[i][j]
            }
        }
    };
    ($map:ident; $point:ident; $cell:ident {$($c:expr => $name:ident,)*};) => {
        charmap! {
            $map;
            $point;
            $cell {$(
                $c => $name
            ),*};
        }
    };
}

/// Extends the functionality of the `scan` module by automatically trimming and
/// converting each yielded slice into a specific type, returning all parsed items
/// as a tuple.
macro_rules! scan {
    ($in:expr, $fmt:expr, ($($type:ty),*)) => {{
        let mut scanner = $crate::util::scan::scan($in, $fmt);
        ($(
            scanner.next().unwrap()
                .trim()
                .parse::<$type>().unwrap(),
        )*)
    }};
}
