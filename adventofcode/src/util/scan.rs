//! Simple pattern-based input parsing.

struct Pattern;

impl Pattern {
    fn parse(_s: &str) -> Pattern {
        Pattern
    }
}

struct Arguments<'a> {
    literals: Vec<&'a str>,
    patterns: Vec<Pattern>,
}

impl<'a> Arguments<'a> {
    fn parse(s: &'a str) -> Arguments<'a> {
        let mut literals = Vec::new();
        let mut patterns = Vec::new();
        let mut i = 0;
        while i < s.len() {
            if let Some(mut j) = s[i..].find("${") {
                j = i + j;
                literals.push(&s[i..j]);
                j = j + 2;
                let k = j + s[j..].find("}")
                    .expect("Non-terminated pattern expression");
                patterns.push(Pattern::parse(&s[j..k]));
                i = k + 1;
            } else {
                literals.push(&s[i..]);
                break;
            }
        }
        Arguments {
            literals,
            patterns,
        }
    }
}

pub struct Scanner<'a> {
    inner: &'a str,
    args: Arguments<'a>,
    i: usize,
    j: usize,
}

impl<'a> Iterator for Scanner<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(&lit) = self.args.literals.get(self.j) {
            if lit != &self.inner[self.i..self.i + lit.len()] {
                panic!("Failed to match pattern");
            }
            self.i += lit.len();
            self.j += 1;
            if let Some(_) = self.args.patterns.get(self.j - 1) {
                let substr = if let Some(nextlit) = self.args.literals.get(self.j) {
                    let k = self.i + self.inner[self.i..].find(nextlit)
                        .expect("Failed to match pattern");
                    &self.inner[self.i..k]
                } else {
                    &self.inner[self.i..]
                };
                self.i += substr.len();
                Some(substr)
            } else {
                None
            }
        } else {
            None
        }
    }
}

pub fn scan<'a>(input: &'a str, fmt: &'a str) -> Scanner<'a> {
    Scanner {
        inner: input,
        args: Arguments::parse(fmt),
        i: 0,
        j: 0,
    }
}
