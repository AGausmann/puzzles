//! Graph-based algorithms.

use std::collections::{HashMap, HashSet};
use std::hash::Hash;

/// Finds the shortest path from a starting node to a target using the
/// A* (A-Star) search algorithm.
///
/// If no path can be created that reaches a target, `None` is returned. Otherwise,
/// it returns the sum of the `g` values between each pair of nodes in the path from the
/// start to the target, as well as each node in order from the path taken to the target
/// (excluding the start node and including the chosen target).
///
/// Instead of accepting and handling a collection of targets, the function accepts a
/// generic `is_target` function where you can pass the appropriate `contains` method
/// from your own collection type.
///
/// This generic approach also means that some work has to be done when writing `hfunc`
/// - this algorithm assumes that the passed function will return the minimum heuristic
/// cost between the given node and any target node.
pub fn astar<Node, IsTarget, Neighbors, NeighborsFn, G, H>(
    start: Node,
    mut is_target: IsTarget,
    mut neighbors: NeighborsFn,
    mut gfunc: G,
    mut hfunc: H
)
    -> Option<(usize, Vec<Node>)>
where
    Node: PartialEq + Eq + Hash + Clone,
    IsTarget: FnMut(&Node) -> bool,
    NeighborsFn: FnMut(&Node) -> Neighbors,
    Neighbors: IntoIterator<Item=Node>,
    G: for<'a> FnMut(&'a Node, &'a Node) -> usize,
    H: for<'a> FnMut(&'a Node) -> usize,
{
    let mut memo_h = HashMap::new();
    let mut hfunc = |node: &Node| {
        *memo_h.entry(node.clone()).or_insert_with(|| hfunc(node))
    };

    let mut closed_set = HashSet::new();
    let mut min_g = HashMap::new();
    let mut parents = HashMap::new();
    min_g.insert(start, 0);

    while let Some((current_node, &current_g)) = min_g.iter()
        .min_by_key(|&(node, &g)| g + hfunc(node))
    {
        let current_node = current_node.clone();
        if is_target(&current_node) {
            let mut chain = Vec::new();
            let mut tail = current_node;
            while let Some(parent) = parents.remove(&tail) {
                chain.push(tail);
                tail = parent;
            }
            chain.reverse();
            return Some((current_g, chain));
        }

        for neighbor_node in neighbors(&current_node) {
            if closed_set.contains(&neighbor_node) {
                continue;
            }

            let neighbor_g = current_g + gfunc(&current_node, &neighbor_node);
            if !min_g.contains_key(&neighbor_node)
                || neighbor_g < min_g[&neighbor_node]
            {
                parents.insert(neighbor_node.clone(), current_node.clone());
                min_g.insert(neighbor_node, neighbor_g);
            }
        }

        min_g.remove(&current_node);
        closed_set.insert(current_node);
    }
    None
}
