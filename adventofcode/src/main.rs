extern crate adventofcode;
use adventofcode::current_year;

use std::env::args;
use std::io::{stdin, Read};

fn main() {
    let day: usize = args()
        .nth(1).unwrap()
        .parse().unwrap();
    let mut input = String::new();
    stdin().read_to_string(&mut input)
        .expect("Unable to read from stdin");

    let solution = current_year::DAYS[day](&input);
    println!("{:?}", solution);
}
