pub mod day01;
pub mod day02;
pub mod day03;
pub mod day04;
pub mod day05;
pub mod day06;
pub mod day07;
pub mod day08;
pub mod day09;
pub mod day10;
pub mod day11;
pub mod day12;
pub mod day13;
pub mod day14;
pub mod day15;
pub mod day16;
pub mod day17;
pub mod day18;
pub mod day19;
pub mod day20;
pub mod day21;
pub mod day22;
pub mod day23;
pub mod day24;
pub mod day25;

fn day00(_input: &str) -> (String, String) {
    panic!()
}

pub const DAYS: [fn(&str) -> (String, String); 26] = [
    day00,
    day01::solution,
    day02::solution,
    day03::solution,
    day04::solution,
    day05::solution,
    day06::solution,
    day07::solution,
    day08::solution,
    day09::solution,
    day10::solution,
    day11::solution,
    day12::solution,
    day13::solution,
    day14::solution,
    day15::solution,
    day16::solution,
    day17::solution,
    day18::solution,
    day19::solution,
    day20::solution,
    day21::solution,
    day22::solution,
    day23::solution,
    day24::solution,
    day25::solution,
];

