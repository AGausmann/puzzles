pub fn solution(input: &str) -> (String, String) {
    let claims = input
        .lines()
        .map(|line| scan!(
            line,
            "#${} @ ${},${}: ${}x${}",
            (usize, usize, usize, usize, usize)
        ))
        .collect::<Vec<_>>();

    let m = 1000;
    let n = 1000;
    let mut map: Vec<Vec<usize>> = vec![vec![0; m]; n];

    claims
        .iter()
        .for_each(|&(_, x, y, w, h)|
            map[y..y+h]
                .iter_mut()
                .for_each(|row|
                    row[x..x+w]
                        .iter_mut()
                        .for_each(|x| *x += 1)
                )
        );

    let solution_a = map
        .iter()
        .flatten()
        .filter(|&&x| x >= 2)
        .count()
        .to_string();

    let solution_b = claims
        .iter()
        .find(|&&(_, x, y, w, h)|
            map[y..y+h]
                .iter()
                .all(|row|
                     row[x..x+w]
                        .iter()
                        .all(|&x| x == 1)
                )
        )
        .unwrap()
        .0
        .to_string();

    (solution_a, solution_b)
}

#[test]
fn examples() {
    assert_eq!(
        solution(indoc!("
            #1 @ 1,3: 4x4
            #2 @ 3,1: 4x4
            #3 @ 5,5: 2x2
        ")),
        (
            "4".to_string(),
            "3".to_string(),
        ),
    );
    assert_eq!(
        solution(include_str!("day03.in")),
        (
            "121259".to_string(),
            "239".to_string(),
        ),
    );
}
