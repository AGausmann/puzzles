charmap! {
    Cave;
    Point;
    Cell {
        '#' => Wall,
        '.' => Open,
        'G' => Goblin,
        'E' => Elf,
    };
}

pub fn solution(_input: &str) -> (String, String) {
    ("".to_string(), "".to_string())
}

#[ignore]
#[test]
fn examples() {
}
