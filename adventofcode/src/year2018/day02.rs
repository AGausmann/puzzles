use std::collections::BTreeMap;

fn hamming(a: &str, b: &str) -> usize {
    a.chars()
        .zip(b.chars())
        .filter(|&(ac, bc)| ac != bc)
        .count()
}

pub fn solution(input: &str) -> (String, String) {
    let box_ids: Vec<String> = input
        .lines()
        .map(str::to_string)
        .collect();

    let letter_counts: Vec<BTreeMap<char, usize>> = box_ids
        .iter()
        .map(|id| {
            id
                .chars()
                .fold(
                    BTreeMap::new(),
                    |mut map, c| {
                        *map.entry(c).or_insert(0) += 1;
                        map
                    }
                )
        })
        .collect();

    let twos = letter_counts
        .iter()
        .filter(|map| {
            map
                .values()
                .any(|&v| v == 2)
        })
        .count();
    let threes = letter_counts
        .iter()
        .filter(|map| {
            map
                .values()
                .any(|&v| v == 3)
        })
        .count();

    let solution_a = (twos * threes).to_string();

    let (id_a, id_b) = box_ids
        .iter()
        .flat_map(|id_a| {
            box_ids
                .iter()
                .map(move |id_b| (id_a, id_b))
        })
        .find(|&(a, b)| hamming(a, b) == 1)
        .unwrap();

    let mut chars = id_a.chars()
        .zip(id_b.chars());

    let prefix: String = chars
        .by_ref()
        .take_while(|&(a, b)| a == b)
        .map(|(a, _)| a)
        .collect();

    let suffix: String = chars
        // implicit skip(1) consumed by previous take_while
        .map(|(a, _)| a)
        .collect();

    let solution_b = format!("{}{}", prefix, suffix);

    (solution_a, solution_b)
}

#[test]
fn examples() {
    assert_eq!(
        solution("abcdef\nbababc\nabbcde\nabcccd\naabcdd\nabcdee\nababab\n").0,
        "12"
    );
    assert_eq!(
        solution("abcde\nfghij\nklmno\npqrst\nfguij\naxcye\nwvxyz\n").1,
        "fgij"
    );
    assert_eq!(
        solution(include_str!("day02.in")).0,
        "5681"
    );
    assert_eq!(
        solution(include_str!("day02.in")).1,
        "uqyoeizfvmbistpkgnocjtwld"
    );
}
