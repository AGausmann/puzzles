use std::collections::BTreeSet;

pub fn solution(input: &str) -> (String, String) {
    let diffs: Vec<isize> = input
        .lines()
        .map(|line| line.parse().unwrap())
        .collect();

    let solution_a = diffs
        .iter()
        .sum::<isize>()
        .to_string();

    let mut freq = 0;
    let mut seen = BTreeSet::new();
    let solution_b = diffs
        .iter()
        .cycle()
        .map(|&diff| {
            let old_freq = freq;
            freq += diff;
            old_freq
        })
        .find(|&freq| {
            let is_new = seen.insert(freq);
            !is_new
        })
        .unwrap()
        .to_string();

    (solution_a, solution_b)
}

#[test]
fn examples() {
    assert_eq!(
        solution("+1\n-2\n+3\n+1\n").0,
        "3"
    );
    assert_eq!(
        solution("+1\n-2\n+3\n+1\n").1,
        "2"
    );
    assert_eq!(
        solution(include_str!("day01.in")).0,
        "510"
    );
    assert_eq!(
        solution(include_str!("day01.in")).1,
        "69074"
    );
}
