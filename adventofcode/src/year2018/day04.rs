use std::collections::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Log {
    Shift(usize),
    Sleep,
    Wake,
}

impl std::str::FromStr for Log {
    type Err = ();

    fn from_str(s: &str) -> Result<Log, Self::Err> {
        match s {
            "falls asleep" => Ok(Log::Sleep),
            "wakes up" => Ok(Log::Wake),
            _ => Ok(Log::Shift(scan!(s, "Guard #${} begins shift", (usize)).0)),
        }
    }
}

pub fn solution(input: &str) -> (String, String) {
    let mut logs = input
        .lines()
        .map(|line| scan!(
            line,
            "[${}-${}-${} ${}:${}] ${}",
            (usize, usize, usize, usize, usize, Log)
        ))
        .collect::<Vec<_>>();
    logs.sort_by_key(|&(y, m, d, h, n, _)| (y, m, d, h, n));

    let mut guard = 0;
    let mut start_minute = 0;
    let mut guard_sleep_totals = BTreeMap::new();
    let mut guard_sleep_freq = BTreeMap::new();
    logs
        .iter()
        .for_each(|&(_, _, _, _, n, log)| match log {
            Log::Shift(new_guard) => guard = new_guard,
            Log::Sleep => {
                start_minute = n;
            },
            Log::Wake => {
                *guard_sleep_totals.entry(guard).or_insert(0) += n - start_minute;
                guard_sleep_freq
                    .entry(guard)
                    .or_insert(vec![0; 60])
                    [start_minute..n]
                    .iter_mut()
                    .for_each(|x| *x += 1);
            }
        });

    let chosen_guard = guard_sleep_totals
        .iter()
        .max_by_key(|(_, &x)| x)
        .unwrap()
        .0;

    let chosen_minute = guard_sleep_freq[chosen_guard]
        .iter()
        .enumerate()
        .max_by_key(|(_, &x)| x)
        .unwrap()
        .0;

    let solution_a = (chosen_guard * chosen_minute)
        .to_string();

    let (chosen_guard, (chosen_minute, _)) = guard_sleep_freq
        .iter()
        .map(|(&guard, freqs)|
             (
                 guard,
                 freqs
                    .iter()
                    .enumerate()
                    .max_by_key(|&(_, &x)| x)
                    .unwrap(),
            )
        )
        .max_by_key(|&(_, (_, &x))| x)
        .unwrap();

    let solution_b = (chosen_guard * chosen_minute)
        .to_string();

    (solution_a, solution_b)
}

#[test]
fn examples() {
    assert_eq!(
        solution(indoc!("
            [1518-11-01 00:00] Guard #10 begins shift
            [1518-11-01 00:05] falls asleep
            [1518-11-01 00:25] wakes up
            [1518-11-01 00:30] falls asleep
            [1518-11-01 00:55] wakes up
            [1518-11-01 23:58] Guard #99 begins shift
            [1518-11-02 00:40] falls asleep
            [1518-11-02 00:50] wakes up
            [1518-11-03 00:05] Guard #10 begins shift
            [1518-11-03 00:24] falls asleep
            [1518-11-03 00:29] wakes up
            [1518-11-04 00:02] Guard #99 begins shift
            [1518-11-04 00:36] falls asleep
            [1518-11-04 00:46] wakes up
            [1518-11-05 00:03] Guard #99 begins shift
            [1518-11-05 00:45] falls asleep
            [1518-11-05 00:55] wakes up
        ")),
        (
             "240".to_string(),
             "4455".to_string(),
        ),
    );
    assert_eq!(
        solution(include_str!("day04.in")),
        (
            "60438".to_string(),
            "47989".to_string(),
        ),
    );
}
