use std::cmp::Reverse;
use std::rc::Rc;
use std::cell::RefCell;
use std::collections::*;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Group {
    pub units: usize,
    pub hp: usize,
    pub attack: usize,
    pub attack_type: String,
    pub initiative: usize,
    pub weaknesses: BTreeSet<String>,
    pub immunities: BTreeSet<String>,
}

impl Group {
    pub fn power(&self) -> usize {
        self.units * self.attack
    }

    pub fn attack_damage(&self, victim: &Group) -> usize {
        if victim.immunities.contains(&self.attack_type) {
            return 0;
        }

        let mut damage = self.power();
        if  victim.weaknesses.contains(&self.attack_type) {
            damage *= 2;
        }
        damage
    }

    pub fn select_target(&self, targets: &mut Vec<Rc<RefCell<Group>>>) -> Option<Rc<RefCell<Group>>> {
        targets.sort_by_key(|target| (self.attack_damage(&target.borrow()), target.borrow().power(), target.borrow().initiative));
        if let Some(target) = targets.last() {
            if self.attack_damage(&target.borrow()) == 0 {
                return None
            }
        }
        targets.pop()
    }

    pub fn attack(&self, target: &mut Group) {
        target.units = target.units.saturating_sub(self.attack_damage(target) / target.hp);
    }

    pub fn is_dead(&self) -> bool {
        self.units == 0
    }
}

impl std::str::FromStr for Group {
    type Err = ();

    fn from_str(s: &str) -> Result<Group, Self::Err> {
        let (units, hp, effects, attack, attack_type, initiative) = scan!(
            s,
            "${} units each with ${} hit points ${}with an attack that does ${} ${} damage at initiative ${}",
            (usize, usize, String, usize, String, usize)
        );

        let mut weaknesses = BTreeSet::new();
        let mut immunities = BTreeSet::new();
        if !effects.is_empty() {
            let effects = &effects[1..effects.len() - 1];
            let clauses = effects.split("; ");
            for clause in clauses {
                if clause.starts_with("immune to ") {
                    immunities.extend(
                        clause["immune to ".len()..]
                        .split(", ")
                        .map(str::to_string)
                    );
                } else if clause.starts_with("weak to ") {
                    weaknesses.extend(
                        clause["weak to ".len()..]
                        .split(", ")
                        .map(str::to_string)
                    );
                } else {
                    panic!("Unrecognized effect clause {}", clause);
                }
            }
        };

        Ok(Group {
            units,
            hp,
            attack,
            attack_type,
            initiative,
            weaknesses,
            immunities,
        })
    }
}

fn run(system_groups: &[Group], infection_groups: &[Group]) -> (bool, usize) {
    let mut system_groups: Vec<_> = system_groups
        .iter()
        .map(|target| Rc::new(RefCell::new(target.clone())))
        .collect();
    let mut infection_groups: Vec<_> = infection_groups
        .iter()
        .map(|target| Rc::new(RefCell::new(target.clone())))
        .collect();

    while !system_groups.is_empty() && !infection_groups.is_empty() {
        // Target selection phase
        system_groups.sort_by_key(|group| Reverse((group.borrow().power(), group.borrow().initiative)));
        infection_groups.sort_by_key(|group| Reverse((group.borrow().power(), group.borrow().initiative)));

        let mut system_target_choices = infection_groups.clone();
        let mut infection_target_choices = system_groups.clone();

        let mut selected_targets = system_groups
            .iter()
            .map(|group| (Rc::clone(group), group.borrow().select_target(&mut system_target_choices)))
        .chain(infection_groups
            .iter()
            .map(|group| (Rc::clone(group), group.borrow().select_target(&mut infection_target_choices)))
        )
            .collect::<Vec<_>>();

        // Attack phase
        let current_count = system_groups
            .iter()
            .chain(&infection_groups)
            .map(|group| group.borrow().units)
            .sum::<usize>();

        selected_targets.sort_by_key(|(group, _target)| Reverse(group.borrow().initiative));
        for (group, target) in &selected_targets {
            if let Some(target) = target {
                group.borrow().attack(&mut target.borrow_mut());
            }
        }

        system_groups.retain(|group| !group.borrow().is_dead());
        infection_groups.retain(|group| !group.borrow().is_dead());

        let after_count = system_groups
            .iter()
            .chain(&infection_groups)
            .map(|group| group.borrow().units)
            .sum::<usize>();

        if after_count == current_count {
            // Stalemate
            return (false, 0);
        }
    }

    let leftover = system_groups
        .iter()
        .chain(&infection_groups)
        .map(|group| group.borrow().units)
        .sum::<usize>();

    (!system_groups.is_empty(), leftover)
}

pub fn run_with_bias(system_groups: &[Group], infection_groups: &[Group], bias: usize)
    -> (bool, usize)
{
    let system_groups = system_groups
        .iter()
        .map(|group| {
            let mut group = group.clone();
            group.attack += bias;
            group
        })
        .collect::<Vec<_>>();

    run(&system_groups, &infection_groups)
}

pub fn solution(input: &str) -> (String, String) {
    let mut lines = input.lines();
    let system_groups = lines
        .by_ref()
        .skip(1)
        .take_while(|s| !s.is_empty())
        .map(|line| line.parse::<Group>().unwrap())
        .collect::<Vec<_>>();

    let infection_groups = lines
        .by_ref()
        .skip(1)
        .map(|line| line.parse::<Group>().unwrap())
        .collect::<Vec<_>>();

    let solution_a = run(&system_groups, &infection_groups)
        .1
        .to_string();

    let solution_b = (1..)
        .map(|i| run_with_bias(&system_groups, &infection_groups, i))
        .find(|&(b, _)| b)
        .unwrap()
        .1
        .to_string();

    (solution_a, solution_b)
}

#[test]
fn examples() {
    assert_eq!(
        solution(indoc!("
            Immune System:
            17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
            989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3

            Infection:
            801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
            4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4
        ")),
        (
            "5216".to_string(),
            "51".to_string(),
        ),
    );
    assert_eq!(
        solution(include_str!("day24.in")),
        (
            "15919".to_string(),
            "354".to_string(),
        ),
    );
}
