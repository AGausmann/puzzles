#[macro_use]
extern crate indoc;
#[macro_use]
extern crate maplit;
extern crate rayon;

#[macro_use]
mod macros;

pub mod util;
pub mod year2018;

pub use self::year2018 as current_year;
