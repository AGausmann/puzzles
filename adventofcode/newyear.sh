#!/bin/sh

year="${1:-$(date +%Y)}"
srcdir="$(dirname "$0")/src"
yeardir="$srcdir/year$year"

if [ -e "$yeardir" ]
then
    echo "Directory $yeardir already exists. To overwrite, remove it first."
    exit 1
fi

mkdir -p "$yeardir"
cp "$srcdir/year.rs.in" "$yeardir/mod.rs"
for i in {01..25}
do
    cp "$srcdir/day.rs.in" "$yeardir/day$i.rs"
done

echo "Module created in $yeardir. To activate, edit $srcdir/lib.rs and add it to the module list."
